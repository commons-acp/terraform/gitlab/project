terraform {

  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = ">= 3.4"
    }
  }
}

// Create a project in the example group
resource "gitlab_project" "projet" {
  name         = var.name
  description  = "An empty project"
  namespace_id = var.gitlab_group_id
  initialize_with_readme = false
  shared_runners_enabled = false
  default_branch = "master"
  visibility_level = "public"
  

}
resource "null_resource" "initialize_repo" {
  provisioner "local-exec" {
    command = "/usr/bin/python3.8 $PATH/initialize.py $TOKEN $PROJECT_ID"

    environment = {
      PATH = path.module
      TOKEN = var.gitlab_token
      PROJECT_ID = gitlab_project.projet.id
    }
  }
}
