variable "gitlab_group_id" {
  description = "group id of the project"
}
variable "name"{
  description = "project name"
}
variable "gitlab_token"{
  description = "gitlab user token"
}
