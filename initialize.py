import requests
import sys
import gitlab
from string import Template

gitlab_token = sys.argv[1]
project_id = sys.argv[2]

gl = gitlab.Gitlab("https://gitlab.com",private_token = gitlab_token)

project = gl.projects.get(project_id)
project_name = project.attributes.get("path")
project_group = project.attributes.get("namespace")['name']



d = {
    'project_id' : project_id,
    'project_name' : project_name,
    'project_group' : project_group
}
gitignore_content   = Template(requests.get("https://gitlab.com/commons-acp/templates/gitlab/maven/-/raw/master/.gitignore").text).substitute(d)
gitlab_ci_content   = Template(requests.get("https://gitlab.com/commons-acp/templates/gitlab/angular/-/raw/main/.gitlab-ci.yml").text).substitute(d)
pom_xml_content     = Template(requests.get("https://gitlab.com/commons-acp/templates/gitlab/maven/-/raw/master/pom.xml").text).substitute(d)


data= {
    "branch": "master",
    "commit_message": "initialize repo",
    "start_branch": "master",
    "actions": [
        {
            "action": "create",
            "file_path": ".gitignore",
            "content": gitignore_content
        },
        {
            "action": "create",
            "file_path": ".gitlab-ci.yml",
            "content": gitlab_ci_content
        },
        {
            "action": "create",
            "file_path": "pom.xml",
            "content": pom_xml_content
        }
    ]
}

commit = project.commits.create(data)
